msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:47+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/filters/wavelet_decompose.rst:None
msgid ".. image:: images/filters/Wavelet_decompose.png"
msgstr ""

#: ../../reference_manual/filters/wavelet_decompose.rst:1
msgid "Overview of the wavelet decompose in Krita."
msgstr ""

#: ../../reference_manual/filters/wavelet_decompose.rst:11
#: ../../reference_manual/filters/wavelet_decompose.rst:16
msgid "Wavelet Decompose"
msgstr "Décomposition en ondelettes"

#: ../../reference_manual/filters/wavelet_decompose.rst:18
msgid ""
"Wavelet decompose uses wavelet scales to turn the current layer into a set "
"of layers with each holding a different type of pattern that is visible "
"within the image. This is used in texture and pattern making to remove "
"unwanted noise quickly from a texture."
msgstr ""

#: ../../reference_manual/filters/wavelet_decompose.rst:20
msgid "You can find it under :menuselection:`Layers`."
msgstr ""

#: ../../reference_manual/filters/wavelet_decompose.rst:22
msgid ""
"When you select it, it will ask for the amount of wavelet scales. More "
"scales, more different layers. Press :guilabel:`OK`, and it will generate a "
"group layer containing the layers with their proper blending modes:"
msgstr ""

#: ../../reference_manual/filters/wavelet_decompose.rst:27
msgid ""
"Adjust a given layer with middle gray to neutralize it, and merge everything "
"with the :guilabel:`Grain Merge` blending mode to merge it into the end "
"image properly."
msgstr ""
