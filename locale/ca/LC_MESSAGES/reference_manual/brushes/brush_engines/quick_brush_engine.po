# Translation of docs_krita_org_reference_manual___brushes___brush_engines___quick_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:46+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:1
msgid "The Quick Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell ràpid."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:15
msgid "Quick Brush Engine"
msgstr "Motor del pinzell ràpid"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:19
msgid ".. image:: images/icons/quickbrush.svg"
msgstr ".. image:: images/icons/quickbrush.svg"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:20
msgid ""
"A Brush Engine inspired by the common artist's workflow where a simple big "
"brush, like a marker, is used to fill large areas quickly, the Quick Brush "
"engine is an extremely simple, but quick brush, which can give the best "
"performance of all Brush Engines."
msgstr ""
"Un motor de pinzell inspirat pel flux de treball dels artistes habituals, on "
"utilitzen un senzill gran pinzell com a marcador, i l'utilitzen per omplir "
"àrees grans de forma ràpida, el motor del Pinzell ràpid és un pinzell "
"extremadament senzill, però ràpid, el qual us donarà el millor rendiment "
"d'entre tots els motors de pinzell."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:22
msgid ""
"It can only change size, blending mode and spacing, and this allows for "
"making big optimisations that aren't possible with other brush engines."
msgstr ""
"Només és possible canviar la mida, el mode de barreja i l'espaiat, el qual "
"permet fer grans optimitzacions que no són possibles amb altres motors de "
"pinzell."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:26
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:27
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:30
msgid "Brush"
msgstr "Pinzell"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:32
msgid "The only parameter specific to this brush."
msgstr "L'únic paràmetre específic d'aquest pinzell."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:34
msgid "Diameter"
msgstr "Diàmetre"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:35
msgid ""
"The size. This brush engine can only make round dabs, but it can make them "
"really fast despite size."
msgstr ""
"La mida. Aquest motor de pinzell només pot aplicar tocs de traç rodons, "
"encara que pot fer-los prou ràpid i independentment de la mida."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid "Spacing"
msgstr "Espaiat"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid ""
"The spacing between the dabs. This brush engine is particular in that it's "
"faster with a lower spacing, unlike all other brush engines."
msgstr ""
"L'espaiat entre els tocs del traç. Aquest motor de pinzell és particular, ja "
"que resulta més ràpid amb un espaiat més baix, a diferència de tots els "
"altres motors de pinzell."

# skip-rule: t-acc_obe
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:40
msgid "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
msgstr "`Tasca al Phabricator <https://phabricator.kde.org/T3492>`_"
