# Translation of docs_krita_org_reference_manual___preferences.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___preferences\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-21 14:18+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/preferences.rst:1
msgid "Krita's settings and preferences options."
msgstr "Параметри роботи Krita."

#: ../../reference_manual/preferences.rst:16
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences.rst:18
msgid ""
"Krita is highly customizable and makes many settings and options available "
"to customize through the Preferences area. These settings are accessed by "
"going to :menuselection:`Settings --> Configure Krita`. On MacOS, the "
"settings are under the topleft menu area, as you would expect of any program "
"under MacOS."
msgstr ""
"У Krita передбачено можливості з гнучкого налаштовування. Багато параметрів "
"є доступними до налаштовування за допомогою вікна налаштувань. Відкрити це "
"вікно можна за допомогою пункту меню :menuselection:`Параметри --> "
"Налаштувати Krita`. У MacOS доступ до налаштувань можна отримати за "
"допомогою ділянки меню вгорі ліворуч у вікні, як і у будь-якій програмі "
"MacOS."

#: ../../reference_manual/preferences.rst:20
msgid ""
"Krita's preferences are saved in the file ``kritarc``. This file is located "
"in ``%LOCALAPPDATA%\\`` on Windows, ``~/.config`` on Linux, and ``~/Library/"
"Preferences`` on OS X. If you would like to back up your custom settings or "
"synchronize them from one computer to another, you can just copy this file. "
"It even works across platforms!"
msgstr ""
"Налаштування Krita зберігаються у файлі ``kritarc``. Цей файл зберігається у "
"каталозі ``%LOCALAPPDATA%\\`` у Windows, у каталозі ``~/.config`` у Linux і "
"у каталозі ``~/Library/Preferences`` у OS X. Якщо ви хочете створити "
"резервну копію ваших нетипових налаштувань або синхронізувати їх з іншим "
"комп'ютером, ви можете просто скопіювати цей файл. Файл можна навіть "
"переносити з однієї програмної платформи на іншу!"

#: ../../reference_manual/preferences.rst:22
msgid ""
"If you have installed Krita through the Windows store, the kritarc file will "
"be in another location:"
msgstr ""
" Якщо ви встановили Krita за допомогою крамниці програм Windows, файл "
"kritarc зберігатиметься у іншому місці::"

#: ../../reference_manual/preferences.rst:24
msgid ""
":file:`%LOCALAPPDATA%\\\\Packages\\\\49800Krita_{RANDOM STRING}\\\\LocalCache"
"\\\\Local\\\\kritarc`"
msgstr ""
":file:`%LOCALAPPDATA%\\\\Packages\\\\49800Krita_{ВИПАДКОВИЙ_РЯДОК}\\"
"\\LocalCache\\\\Local\\\\kritarc`"

#: ../../reference_manual/preferences.rst:27
msgid ""
"Custom shortcuts are saved in a separate file ``kritashortcutsrc`` which can "
"also be backed up in the same way. This is discussed further in the "
"shortcuts section."
msgstr ""
"Нетипові клавіатурні скорочення зберігаються у окремому файлі "
"``kritashortcutsrc``, резервну копію якого можна створити у той самий "
"спосіб. Докладніше обговорення можна знайти у розділі щодо клавіатурних "
"скорочень."
