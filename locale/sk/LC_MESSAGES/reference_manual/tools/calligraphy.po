# translation of docs_krita_org_reference_manual___tools___calligraphy.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___calligraphy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 13:06+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:16
msgid ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: toolcalligraphy"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:1
msgid "Krita's calligraphy tool reference."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Vector"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Path"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Variable Width Stroke"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:11
#, fuzzy
#| msgid "**Calligraphy**"
msgid "Calligraphy"
msgstr "**Kaligrafia**"

#: ../../reference_manual/tools/calligraphy.rst:16
msgid "Calligraphy Tool"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:18
msgid "|toolcalligraphy|"
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:20
msgid ""
"The Calligraphy tool allows for variable width lines, with input managed by "
"the tablet. Press down with the stylus/left mouse button on the canvas to "
"make a line, lifting the stylus/mouse button ends the stroke."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:24
msgid "Tool Options"
msgstr "Voľby nástroja"

#: ../../reference_manual/tools/calligraphy.rst:26
msgid "**Fill**"
msgstr "**Výplň**"

#: ../../reference_manual/tools/calligraphy.rst:28
msgid "Doesn't actually do anything."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:30
msgid "**Calligraphy**"
msgstr "**Kaligrafia**"

#: ../../reference_manual/tools/calligraphy.rst:32
msgid ""
"The drop-down menu holds your saved presets, the :guilabel:`Save` button "
"next to it allows you to save presets."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:34
msgid "Follow Selected Path"
msgstr "Sledovať vybranú cestu"

#: ../../reference_manual/tools/calligraphy.rst:35
msgid ""
"If a stroke has been selected with the default tool, the calligraphy tool "
"will follow this path."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:36
msgid "Use Tablet Pressure"
msgstr "Použiť tlak tabletu"

#: ../../reference_manual/tools/calligraphy.rst:37
msgid "Uses tablet pressure to control the stroke width."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:38
msgid "Thinning"
msgstr "Stenčovanie"

#: ../../reference_manual/tools/calligraphy.rst:39
msgid ""
"This allows you to set how much thinner a line becomes when speeding up the "
"stroke. Using a negative value makes it thicker."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:40
msgid "Width"
msgstr "Šírka"

#: ../../reference_manual/tools/calligraphy.rst:41
msgid "Base width for the stroke."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:42
msgid "Use Tablet Angle"
msgstr "Použiť uhol tabletu"

#: ../../reference_manual/tools/calligraphy.rst:43
msgid ""
"Allows you to use the tablet angle to control the stroke, only works for "
"tablets supporting it."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:44
msgid "Angle"
msgstr "Uhol"

#: ../../reference_manual/tools/calligraphy.rst:45
msgid "The angle of the dab."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:46
msgid "Fixation"
msgstr "Fixácia"

#: ../../reference_manual/tools/calligraphy.rst:47
msgid "The ratio of the dab. 1 is thin, 0 is round."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:48
msgid "Caps"
msgstr "Vrchy"

#: ../../reference_manual/tools/calligraphy.rst:49
msgid "Whether or not an stroke will end with a rounding or flat."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:50
msgid "Mass"
msgstr "Hmotnosť"

#: ../../reference_manual/tools/calligraphy.rst:51
msgid ""
"How much weight the stroke has. With drag set to 0, high mass increases the "
"'orbit'."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:53
msgid "Drag"
msgstr "Chyť"

#: ../../reference_manual/tools/calligraphy.rst:53
msgid ""
"How much the stroke follows the cursor, when set to 0 the stroke will orbit "
"around the cursor path."
msgstr ""

#: ../../reference_manual/tools/calligraphy.rst:57
msgid ""
"The calligraphy tool can be edited by the edit-line tool, but currently you "
"can't add or remove nodes without converting it to a normal path."
msgstr ""
