# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 10:23+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Lut color guilabel image Kritamouseleft dependant\n"
"X-POFile-SpellExtra: Kritamousemiddle kbd Paint repô images LUT alt\n"
"X-POFile-SpellExtra: KritaColorSelectorTypes rabiscos management lut\n"
"X-POFile-SpellExtra: Kritatransformsrecursive icons Kritabasicassistants\n"
"X-POFile-SpellExtra: Tool multibrush HSL CMYK autobrushtip mandala\n"
"X-POFile-SpellExtra: Kritaghostlady HSY ref vinhetas menuselection tool\n"
"X-POFile-SpellExtra: bundle popuppalette Del alpha mouseright kpp Krita\n"
"X-POFile-SpellExtra: destrutivos JPG Kritamouseright HSI iso mousemiddle\n"
"X-POFile-SpellExtra: mouseleft view dockers assistants Painttool tools\n"
"X-POFile-SpellExtra: filters\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: botão do meio"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ".. image:: images/filters/Krita-color-to-alpha.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/dockers/Krita_Color_Selector_Types.png"
msgstr ".. image:: images/dockers/Krita_Color_Selector_Types.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ".. image:: images/tools/Krita-multibrush.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita-view-dependant-lut-management.png"
msgstr ".. image:: images/Krita-view-dependant-lut-management.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/tools/Krita_transforms_recursive.png"
msgstr ".. image:: images/tools/Krita_transforms_recursive.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita_ghostlady_3.png"
msgstr ".. image:: images/Krita_ghostlady_3.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita-popuppalette.png"
msgstr ".. image:: images/Krita-popuppalette.png"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:1
msgid "This is a introduction to Krita for users coming from Paint Tool Sai."
msgstr ""
"Esta é uma introdução ao Krita para os utilizadores que vêm do Paint Tool "
"Sai."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:12
msgid "Sai"
msgstr "Sai"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:12
msgid "Painttool Sai"
msgstr "Painttool Sai"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:17
msgid "Introduction to Krita coming from Paint Tool Sai"
msgstr "Introdução Krita de quem vem do Paint Tool Sai"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:20
msgid "How do you do that in Krita?"
msgstr "Como é que se faz isso no Krita?"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:22
msgid ""
"This section goes over the functionalities that Krita and Paint Tool Sai "
"share, but shows how they slightly differ."
msgstr ""
"Esta secção passa por cima das funcionalidades que o Krita e o Paint Tool "
"Sai partilham, mas mostra em que medida são ligeiramente diferentes."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:25
msgid "Canvas navigation"
msgstr "Navegação pela área de desenho"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:27
msgid ""
"Krita, just like Sai, allows you to flip, rotate and duplicate the view. "
"Unlike Sai, these are tied to keyboard keys."
msgstr ""
"O Krita, tal como o Sai, permite-lhe inverter, rodar e duplicar a vista. Ao "
"contrário do Sai, estas estão associadas a atalhos do teclado."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:29
msgid "Mirror"
msgstr "Espelho"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:30
msgid "This is tied to :kbd:`M` key to flip."
msgstr "Isto está associado ao :kbd:`M` para inverter."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:31
msgid "Rotate"
msgstr "Rodar"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:32
msgid ""
"There's a couple of possibilities here: either the :kbd:`4` and :kbd:`6` "
"keys, or the :kbd:`Ctrl + [` and :kbd:`Ctrl + ]` shortcuts for basic 15 "
"degrees rotation left and right. But you can also have more sophisticated "
"rotation with the :kbd:`Shift + Space + drag` or :kbd:`Shift +` |"
"mousemiddle| :kbd:`+ drag` shortcuts. To reset the rotation, press the :kbd:"
"`5` key."
msgstr ""
"Existem aqui algumas possibilidades: ou o :kbd:`4` e o :kbd:`6`, ou o :kbd:"
"`Ctrl + [` e o :kbd:`Ctrl + ]` para a rotação básica em 15 graus para a "
"esquerda e direita. Mas poderá ainda ter rotações mais sofisticadas com o :"
"kbd:`Shift + Espaço + arrastamento` ou :kbd:`Shift + |mousemiddle| + "
"arrastamento`. Para repor a rotação, carregue em :kbd:`5`."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:34
msgid "Zoom"
msgstr "Ampliação"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:34
msgid ""
"You can use the :kbd:`+` and :kbd:`-` keys to zoom out and in, or use the :"
"kbd:`Ctrl +` |mousemiddle| shortcut. Use the :kbd:`1`, :kbd:`2` or :kbd:`3` "
"keys to reset the zoom, fit the zoom to page or fit the zoom to page width."
msgstr ""
"Poderá usar o :kbd:`+` e o :kbd:`-` para ampliar e reduzir, ou então usar o :"
"kbd:`Ctrl` + |mousemiddle|. Use o :kbd:`1`, o :kbd:`2` ou o kbd:`3` para "
"repor a ampliação, ajustar a ampliação à página ou à largura da mesma."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:36
msgid ""
"You can use the Overview docker in :menuselection:`Settings --> Dockers` to "
"quickly navigate over your image."
msgstr ""
"Poderá usar a área de Visão Geral do :menuselection:`Configuração --> Áreas "
"Acopláveis` para navegar rapidamente pela sua imagem."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:38
msgid ""
"You can also put these commands on the toolbar, so it'll feel a little like "
"Sai. Go to :menuselection:`Settings --> Configure Toolbars`. There are two "
"toolbars, but we'll add to the file toolbar."
msgstr ""
"Também poderá colocar estes comandos na barra de ferramentas, para que se "
"pareça um pouco mais com o Sai. Vá a :menuselection:`Configuração --> "
"Configurar as Barras de Ferramentas`. Existem duas barras de ferramentas, "
"mas vamos adicionar as  opções à barra de ficheiros."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:40
msgid ""
"Then, you can type in something in the left column to search for it. So, for "
"example, 'undo'. Then select the action 'undo freehand stroke' and drag it "
"to the right. Select the action to the right, and click :menuselection:"
"`Change text`. There, toggle :menuselection:`Hide text when toolbar shows "
"action alongside icon` to prevent the action from showing the text. Then "
"press :guilabel:`OK`. When done right, the :guilabel:`Undo` should now be "
"sandwiched between the save and the gradient icon."
msgstr ""
"Depois poderá escrever algo na coluna da esquerda para procurar por ela. Por "
"isso, por exemplo, 'desfazer'. Depois seleccione a acção 'desfazer traço à "
"mão' e arraste-a para a direita. Seleccione a acção à direita e carregue em :"
"menuselection:`Mudar o texto`. Depois, comute a opção :menuselection:"
"`Esconder o texto quando a barra de ferramentas mostra a acção ao lado do "
"ícone` para evitar que a acção mostre o texto. Depois carregue em :guilabel:"
"`OK`. Quando terminar tudo bem, a opção :guilabel:`Desfazer` deverá agora "
"estar rodeada entre o ícone de gravação e do gradiente."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:42
msgid ""
"You can do the same for :guilabel:`Redo`, :guilabel:`Deselect`, :guilabel:"
"`Invert Selection`, :guilabel:`Zoom out`, :guilabel:`Zoom in`, :guilabel:"
"`Reset zoom`, :guilabel:`Rotate left`, :guilabel:`Rotate right`, :guilabel:"
"`Mirror view` and perhaps :guilabel:`Smoothing: basic` and :guilabel:"
"`Smoothing: stabilizer` to get nearly all the functionality of Sai's top bar "
"in Krita's top bar. (Though, on smaller screens this will cause all the "
"things in the brushes toolbar to hide inside a drop-down to the right, so "
"you need to experiment a little)."
msgstr ""
"Poderá fazer o mesmo para o :guilabel:`Refazer`, :guilabel:`Deseleccionar`, :"
"guilabel:`Inverter a Selecção`, :guilabel:`Reduzir`, :guilabel:`Ampliar`, :"
"guilabel:`Repor a ampliação`, :guilabel:`Rodar à esquerda`, :guilabel:`Rodar "
"à direita`, :guilabel:`Vista em espelho` e talvez na :guilabel:`Suavização: "
"básica` e :guilabel:`Suavização: estabilizador` para obter praticamente "
"todas as funcionalidades da barra superior do Sai na barra de topo do Krita. "
"(Todavia, em ecrãs menores, iso fará com que todas as cosias na barra de "
"pincéis fiquem escondidas dentro de uma lista à direita; por isso, terá de "
"experimentar um pouco)."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:44
msgid ""
":guilabel:`Hide Selection`, :guilabel:`Reset Rotation` are currently not "
"available via the Toolbar configuration, you'll need to use the shortcuts :"
"kbd:`Ctrl + H` and :kbd:`5` to toggle these."
msgstr ""
"O :guilabel:`Esconder a Selecção` e o :guilabel:`Repor a Rotação` não estão "
"disponíveis de momento na configuração da Barra de Ferramentas, pelo que "
"terá de usar os atalhos :kbd:`Ctrl + H` e :kbd:`5` para os activar/"
"desactivar."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:48
msgid ""
"Krita 3.0 currently doesn't allow changing the text in the toolbar, we're "
"working on it."
msgstr ""
"O Krita 3.0 não permite de momento a alteração do texto na barra de "
"ferramentas, mas estamos a trabalhar nisso."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:51
msgid "Right click color picker"
msgstr "Selector de cores com o botão direito"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:53
msgid ""
"You can actually set this in :menuselection:`Settings --> Configure Krita --"
"> Canvas input settings --> Alternate invocation`. Just double-click the "
"entry that says :kbd:`Ctrl +` |mouseleft| shortcut before :guilabel:`Pick "
"foreground color from image` to get a window to set it to |mouseright|."
msgstr ""
"Poderá de facto definir isto na opção :menuselection:`Configuração --> "
"Configurar o Krita --> Configuração de entrada da área de desenho --> "
"Invocação alternativa`. Basta fazer duplo-click sobre o elemento que diz :"
"kbd:`Ctrl` + |mouseleft| antes de :guilabel:`Escolher a cor principal da "
"imagem` para obter uma janela onde configurá-la com o |mouseright|."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:57
msgid ""
"Krita 3.0 actually has a Paint-tool Sai-compatible input sheet shipped by "
"default. Combine these with the shortcut sheet for Paint tool Sai to get "
"most of the functionality on familiar hotkeys."
msgstr ""
"O Krita 3.0 tem de facto uma folha de entrada compatível com o Paint Tool "
"Sai por omissão. Combine essa com a folha de atalhos para o Paint tool Sai "
"para obter a maior parte da funcionalidade com atalhos conhecidos."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:60
msgid "Stabilizer"
msgstr "Estabilizador"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:62
msgid ""
"This is in the tool options docker of the freehand brush. Use Basic "
"Smoothing for more advanced tablets, and Stabilizer is much like Paint Tool "
"Sai's. Just turn off :guilabel:`Delay` so that the dead-zone disappears."
msgstr ""
"Isto está na área de opções da ferramenta do pincel à mão. Use a Suavização "
"Básica para as tabletes mais avançadas e o Estabilizador como algo mais "
"próximo do do Paint Tool Sai. Basta desligar o :guilabel:`Atraso` para que "
"as zonas mortas desapareçam."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:65
msgid "Transparency"
msgstr "Transparência"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:67
msgid ""
"So one of the things that throw a lot of Paint Tool Sai users off is that "
"Krita uses checkers to display transparency, which is actually not that "
"uncommon. Still, if you want to have the canvas background to be white, this "
"is possible. Just choose :guilabel:`Background: As Canvas Color` in the new "
"image dialogue and the image background will be white. You can turn it back "
"to transparent via :menuselection:`Image --> Change image background color`. "
"If you export a PNG or JPG, make sure to uncheck :guilabel:`Save "
"transparency` and to make the background color white (it's black by default)."
msgstr ""
"Por isso, uma das coisas que espantam bastante os utilizadores do Paint Tool "
"Sai é que o Krita usa padrões em xadrez para mostrar a transparência, o que "
"não tão fora do normal assim. De qualquer forma, se quiser ter o fundo da "
"área de desenho a branco, isso é possível. Basta escolher :guilabel:`Fundo: "
"Como Cor da Área de Desenho` na janela de nova imagem, para que o fundo da "
"imagem seja o branco. Poderá repô-la como transparente em :menuselection:"
"`Imagem --> Escolher a cor de fundo da imagem`. Se exportar um PNG ou JPG, "
"certifique-se que desligue a opção :guilabel:`Gravar a transparência` e para "
"tornar a cor de fundo como branca (por omissão é preta)."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:72
msgid ""
"Like Sai, you can quickly turn a black and white image to black and "
"transparent with the :menuselection:`Color to Alpha Filter` under :"
"menuselection:`Filters --> Colors --> Color to Alpha`."
msgstr ""
"Como no Sai, poderá transformar rapidamente uma imagem a preto-e-branco numa "
"imagem preta-e-transparente com a opção :menuselection:`Filtro de Cor para "
"Alfa` em :menuselection:`Filtros --> Cores --> Cor para o Alfa`."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:75
msgid "Brush Settings"
msgstr "Configuração do Pincel"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:77
msgid ""
"Another, somewhat amusing misconception is that Krita's brush engine is not "
"very complex. After all, you can only change the Size, Flow and Opacity from "
"the top bar."
msgstr ""
"Outro equívoco às vezes engraçado é que o motor de pincéis do Krita não é "
"muito complexo. Afinal de contas, só poderá alterar o Tamanho, Fluxo e "
"Opacidade na barra de topo."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:79
msgid ""
"This is not quite true. It's rather that we don't have our brush settings in "
"a docker but a drop-down on the toolbar. The easiest way to access this is "
"with the :kbd:`F5` key. As you can see, it's actually quite complex. We have "
"more than a dozen brush engines, which are a type of brush you can make. The "
"ones you are used to from Paint Tool Sai are the Pixel Brush (ink), The "
"Color Smudge Brush (brush) and the filter brush (dodge, burn)."
msgstr ""
"Isto não é bem verdade. É mais no sentido em que não temos as definições do "
"pincel numa área acoplável, mas sim numa lista da barra de ferramentas. A "
"forma mais simples de aceder a esta é com o :kbd:`F5`. Como poderá ver, é de "
"facto bastante complexo. Temos mais de uma dúzia de motores de pincéis, que "
"são um tipo de pincel que poderá criar. Os tipos a que está habituado no "
"Paint Tool Sai são o Pincel de Pixels (pintura), o Pincel de Manchas a Cores "
"(pincel) e o pincel de filtragem (desvio, queimadura)."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:81
msgid ""
"A simple inking brush recipe for example is to take a pixel brush, uncheck "
"the :guilabel:`Enable Pen Settings` on opacity and flow, and uncheck "
"everything but size from the option list. Then, go into brush-tip, pick :ref:"
"`auto_brush_tip` from the tabs, and set the size to 25 (right-click a blue "
"bar if you want to input numbers), turn on anti-aliasing under the brush "
"icon, and set fade to 0.9. Then, as a final touch, set spacing to 'auto' and "
"the spacing number to 0.8."
msgstr ""
"Uma receita simples de pincéis de pintura é pegar num pincel de pixels, "
"desligar a opção :guilabel:`Activar a Configuração da Caneta` sobre a "
"opacidade e o fluxo e desligar tudo menos o tamanho na lista de opções. "
"Depois, vá à ponta do pincel, escolha :ref:`auto_brush_tip` nas páginas e "
"configure o tamanho como 25 (carregue com o botão direito numa barra azul se "
"quiser introduzir números), active a suavização no ícone do pincel e "
"configure o desvanecimento como 0,9. Depois, como toque final, configure o "
"espaço como 'auto' e o número do espaço como 0,8."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:83
msgid ""
"You can configure the brushes in a lot of detail, and share the packs with "
"others. Importing of packs and brushes can be done via the :menuselection:"
"`Settings --> Manage Resources`, where you can import .bundle files or .kpp "
"files."
msgstr ""
"Poderá configurar os pincéis com grande detalhe e partilhá-los com outros. A "
"importação de pacotes de pincéis poderá ser feita com a opção :menuselection:"
"`Configuração --> Gerir os Recursos`, onde poderá importar os ficheiros ."
"bundle ou .kpp."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:86
msgid "Erasing"
msgstr "Apagar"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:88
msgid ""
"Erasing is a blending mode in Krita, much like the transparency mode of "
"Paint Tool Sai. It's activated with the :kbd:`E` key or you can select it "
"from the Blending Mode drop-down..."
msgstr ""
"A limpeza é um modo de mistura no Krita, o que é muito parecido com o modo "
"de transparência do Paint Tool Sai. É activada com o :kbd:`E` ou poderá "
"seleccioná-la a partir da lista do Modo de Mistura..."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:91
msgid "Blending Modes"
msgstr "Modos de Mistura"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:93
msgid ""
"Krita has a lot of Blending modes, and thankfully all of Paint Tool Sai's "
"are amongst them except binary. To manage the blending modes, each of them "
"has a little check-box that you can tick to add them to the favorites."
msgstr ""
"O Krita tem uma grande quantidade de Modos de Mistura, e felizmente todas as "
"do Paint Tool Sai estão incluídas, excepto as binárias. Para gerir os modos "
"de mistura, cada uma delas tem uma pequena opção de marcação que poderá "
"assinalar para as adicionar aos favoritos."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:95
msgid ""
"Multiple, Screen, Overlay and Normal are amongst the favorites. Krita's "
"Luminosity is actually slightly different from Paint Tool Sai's and it "
"replaces the relative brightness of color with the relative brightness of "
"the color of the layer."
msgstr ""
"A Multiplicação, o Ecrã, a Sobreposição e a Normal estão entre as favoritas. "
"A luminosidade do Krita é de facto ligeiramente diferente da do Paint Tool "
"Sai e substitui o brilho relativo da cor com o brilho relativo da cor da "
"camada."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:98
msgid ""
"Sai's Luminosity mode is actually the same as Krita's *Addition* or *linear "
"dodge* mode. The Shade mode is the same as *Color Burn* and *Hard Mix* is "
"the same as the lumi and shade mode."
msgstr ""
"O modo de Luminosidade do Sai é de facto igual ao do modo de *Adição* ou "
"*Desvio Linear* do Krita. O modo de Sombreados é o mesmo que o da "
"*Queimadura de Cores* a *Mistura Forte* é a mesma que o modo de luminosidade "
"e sombras."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:101
msgid "Layers"
msgstr "Camadas"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:103
msgid "Lock Alpha"
msgstr "Bloquear o Alfa"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:104
msgid "This is the checker box icon next to every layer."
msgstr "Este é o ícone do xadrez a seguir a cada camada."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:105
msgid "Clipping group"
msgstr "Recorte de um grupo"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:106
msgid ""
"For Clipping masks in Krita you'll need to put all your images in a single "
"layer, and then press the 'a' icon, or press the :kbd:`Ctrl + Shift + G` "
"shortcut."
msgstr ""
"Para as máscaras de recorte no Krita, terá de colocar todas as suas imagens "
"numa única camada, carregando depois no ícone 'a', ou carregue em :kbd:`Ctrl "
"+ Shift + G`."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:107
msgid "Ink layer"
msgstr "Camada de pintura"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:108
msgid "This is a vector layer in Krita, and also holds the text."
msgstr "Esta é uma camada vectorial no Krita e também contém o texto."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:109
msgid "Masks"
msgstr "Máscaras"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:110
msgid ""
"These grayscale layers that allow you to affect the transparency are called "
"transparency masks in Krita, and like Paint Tool Sai, they can be applied to "
"groups as well as layers. If you have a selection and make a transparency "
"mask, it will use the selection as a base."
msgstr ""
"Estas camadas de níveis de cinzento que lhe permitem afectar a transparência "
"chamam-se máscaras de transparência no Krita e, como no Paint Tool Sai, elas "
"poderão ser aplicadas aos grupos, assim como às camadas. Se tiver uma "
"selecção e criar uma máscara de transparência, ela irá usar a selecção como "
"uma base."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:112
msgid "Clearing a layer"
msgstr "Limpar uma camada"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:112
msgid ""
"This is under :menuselection:`Edit --> Clear`, but you can also just press "
"the :kbd:`Del` key."
msgstr ""
"Isto encontra-se em :menuselection:`Editar --> Limpar`, mas também poderá "
"carregar em :kbd:`Del`."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:115
msgid "Mixing between two colors"
msgstr "Mistura entre duas cores"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:117
msgid ""
"If you liked this docker in Paint Tool Sai, Krita's Digital Color Selector "
"docker will be able to help you. Dragging the sliders will change how much "
"of a color is mixed in."
msgstr ""
"Se gostou desta área acoplável no Paint Tool Sai, a área do Selector de "
"Cores Digitais do Krita podê-lo-á ajudar. Se arrastar as barras, irá "
"modificar a quantidade da cor que é misturada."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:120
msgid "What do you get extra when using Krita?"
msgstr "O que obtém como extra ao usar o Krita?"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:123
msgid "More brush customization"
msgstr "Mais personalizações dos pincéis"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:125
msgid ""
"You already met the brush settings editor. Sketch brushes, grid brushes, "
"deform brushes, clone brushes, brushes that are textures, brushes that "
"respond to tilt, rotation, speed, brushes that draw hatches and brushes that "
"deform the colors. Krita's variety is quite big."
msgstr ""
"Já descobriu o editor de configurações dos pincéis. Os pincéis de rabiscos, "
"os pincéis de grelhas, os pincéis de deformação, de clonagens, de texturas e "
"os pincéis que respondem aos desvios, rotações, velocidade, pincéis que "
"desenham padrões tracejados ou os pincéis que deformam as cores. A variedade "
"do Krita é muito grande."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:128
msgid "More color selectors"
msgstr "Mais selectores de cores"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:130
msgid ""
"You can have HSV sliders, RGB sliders, triangle in a hue ring. But you can "
"also have HSI, HSL or HSY' sliders, CMYK sliders, palettes, round selectors, "
"square selectors, tiny selectors, big selectors, color history and shade "
"selectors. Just go into :menuselection:`Settings --> Configure Krita --> "
"Advanced Color Selector Settings` to change the shape and type of the main "
"big color selector."
msgstr ""
"Poderá ter barras de HSV, barras de RGB e o triângulo num anel de "
"tonalidades. Mas também poderá ter as barras de HSI, HSL ou HSY', as barras "
"do CMYK, paletas, selectores redondos, selectores quadrados, selectores "
"pequenos, selectores grandes, históricos de cores e selectores de "
"sombreados. Basta ir a :menuselection:`Configuração --> Configurar o Krita --"
"> Configuração do Selector de Cores Avançadas` para modificar a forma e tipo "
"do selector de cores principal."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:135
msgid ""
"You can call the color history with the :kbd:`H` key, common colors with "
"the :kbd:`U` key and the two shade selectors with the :kbd:`Shift + N` and :"
"kbd:`Shift + M` shortcuts. The big selector can be called with the :kbd:"
"`Shift + I` shortcut on canvas."
msgstr ""
"Poderá invocar o histórico de cores com o :kbd:`H`, as cores comuns com o :"
"kbd:`U` e os dois selectores de sombras com o :kbd:`Shift + N` e o :kbd:"
"`Shift + M`. O selector grande pode ser invocado com o :kbd:`Shift + I` na "
"área de desenho."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:138
msgid "Geometric Tools"
msgstr "Ferramentas Geométricas"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:140
msgid "Circles, rectangles, paths, Krita allows you to draw these easily."
msgstr ""
"Círculos, rectângulos, caminhos, o Krita permite-lhe desenhá-los com "
"facilidade."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:143
msgid "Multibrush, Mirror Symmetry and Wrap Around"
msgstr "Multi-pincéis, Simetria em Espelho e Envolvência"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:145
msgid ""
"These tools allow you to quickly paint a mirrored image, mandala or tiled "
"texture in no time. Useful for backgrounds and abstract vignettes."
msgstr ""
"Estas ferramentas permitem-lhe pintar rapidamente uma imagem em espelho, uma "
"mandala ou textura em padrão em pouco tempo. Útil para fundos e vinhetas "
"abstractas."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:151
msgid "Assistants"
msgstr "Assistentes"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:153
msgid ""
"The painting assistants can help you to set up a perspective, or a "
"concentric circle and snap to them with the brush."
msgstr ""
"Os assistentes de pintura podê-lo-ão ajudar a configurar uma perspectiva ou "
"um círculo concêntrico e ajustar-se a eles com o pincel."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:159
msgid ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Krita's vanishing point assistants in action"
msgstr ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Os assistentes do ponto de fuga do Krita em acção"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:159
msgid "Krita's vanishing point assistants in action"
msgstr "Os assistentes do ponto de fuga do Krita em acção"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:162
msgid "Locking the Layer"
msgstr "Bloquear a Camada"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:164
msgid "Lock the layer with the padlock so you don't draw on it."
msgstr ""
"Bloqueie a camada com o cadeado, para que não possa desenhar mais sobre ela."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:167
msgid "Quick Layer select"
msgstr "Selecção rápida de camadas"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:169
msgid ""
"If you hold the :kbd:`R` key and press a spot on your drawing, Krita will "
"select the layer underneath the cursor. Really useful when dealing with a "
"large number of layers."
msgstr ""
"Se mantiver carregado o :kbd:`R` e carregar num ponto do seu desenho, o "
"Krita irá seleccionar a camada por baixo do cursor. É bastante útil quando "
"lidar com um grande número de camadas."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:172
msgid "Color Management"
msgstr "Gestão de Cores"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:174
msgid ""
"This allows you to prepare your work for print, or to do tricks with the LUT "
"docker so you can diagnose your image better. For example, using the LUT "
"docker to turn the colors grayscale in a separate view, so you can see the "
"values instantly."
msgstr ""
"Isto permite-lhe preparar o seu trabalho para impressões, ou para fazer "
"truques com a área do Lut, para que possa diagnosticar melhor a sua imagem. "
"Por exemplo, se usar a área do LUT para transformar as cores em tons de "
"cinzento numa janela separada, para que possa ver os valores "
"instantaneamente."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:180
msgid "Advanced Transform Tools"
msgstr "Ferramentas de Transformação Avançadas"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:182
msgid ""
"Not just rotate and scale, but also cage, wrap, liquify and non-destructive "
"transforms with the transform tool and masks."
msgstr ""
"Não só rodar e escalar, mas também envolver, liquidificar e efectuar outras "
"transformações não-destrutivas com a ferramenta e as máscaras de "
"transformação."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:188
msgid "More Filters and non-destructive filter layers and masks"
msgstr "Mais filtros e camadas e máscaras de filtros não-destrutivos"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:190
msgid ""
"With filters like color balance and curves you can make easy shadow layers. "
"In fact, with the filter layers and layer masks you can make them apply on "
"the fly as you draw underneath."
msgstr ""
"Com os filtros de balanceamento de cores e curvas, poderá facilmente criar "
"camadas de sombras. De facto. Com as camadas de filtros e as máscaras de "
"filtragem, podê-las-á aplicar na hora à medida que for desenhando por baixo."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:196
msgid "Pop-up palette"
msgstr "Paleta instantânea"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:198
msgid ""
"This is the little circular thing that is by default on the right click. You "
"can organize your brushes in tags, and use those tags to fill up the pop-up "
"palette. It also keeps a little color selector and color history, so you can "
"switch brushes on the fly."
msgstr ""
"Isto é aquela pequena área circular que aparece por omissão com o botão "
"direito do rato. Poderá organizar os seus pincéis por marcas e usá-las para "
"preencher a paleta instantânea. Também mantém um pequeno selector de cores, "
"bem como um histórico de cores, para que possa mudar de pincéis na hora."

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:204
msgid "What does Krita lack compared to Paint Tool Sai?"
msgstr "O que falta ao Krita em comparação com o Paint Tool Sai?"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:206
msgid "Variable width vector lines"
msgstr "Linhas vectoriais de espessura variável"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:207
msgid "The selection source option for layers"
msgstr "A opção de fonte de selecção para as camadas"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:208
msgid "Dynamic hard-edges for strokes (the fringe effect)"
msgstr "Arestas vincadas dinâmicas para os traços"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:209
msgid "No mix-docker"
msgstr "Nenhuma área de mistura"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:210
msgid "No Preset-tied stabilizer"
msgstr "Sem estabilizador associado a Predefinições"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:211
msgid "No per-preset hotkeys"
msgstr "Sem atalhos de teclado por predefinição"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:214
msgid "Conclusion"
msgstr "Conclusão"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:216
msgid ""
"I hope this introduction got you a little more excited to use Krita, if not "
"feel a little more at home."
msgstr ""
"Espero que esta introdução o tenha convencido um pouco mais a usar o Krita, "
"ou pelo menos ajudá-lo a sentir-se um pouco mais em casa."
