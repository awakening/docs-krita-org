# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:00+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolpatternedit icons image patterntool images alt\n"
"X-POFile-SpellExtra: guilabel\n"

#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: ferramenta de edição de padrões"

#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr "A referência da ferramenta de edição de padrões vectoriais."

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Pattern"
msgstr "Padrão"

#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr "Ferramenta de Edição dos Padrões"

#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr "|toolpatternedit|"

#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""
"A ferramenta de edição dos padrões foi removida no 4.0, sendo que não há "
"forma de momento de editar os preenchimentos com padrões para os vectores."

#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""
"A ferramenta de edição do Padrão funciona nas Formas Vectoriais que usam um "
"preenchimento com um Padrão. A Ferramenta de Edição do Padrão permite-lhe "
"alterar o tamanho, as proporções de tamanho e a origem de um dado padrão."

#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr "Edição na Área de Desenho"

#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""
"Poderá alterar a origem se arrastar o nó superior; isto só é possível no "
"modo Lado-a-Lado."

#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""
"Poderá alterar o tamanho e as suas proporções se arrastar o nó de baixo. Não "
"existe forma de restringir as proporções de tamanho com a edição na própria "
"área de desenho, sendo só possível no modo Original e Lado-a-Lado."

#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr "Existem diversas opções nesta ferramenta para afinar alguns detalhes:"

#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr "Primeiro existem as opções do Padrão."

#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr "Esta opção pode ser igual a:"

#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr "Original:"

#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr "Isto irá mostrar uma única cópia não esticada do padrão."

#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr "Lado-a-Lado (Por Omissão):"

#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr ""
"Isto fará com que o padrão apareça repetido ao longo da direcção em X e Y."

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr "Repetição:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr "Esticamento:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr "Isto irá esticar a imagem do Padrão à forma."

#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr "A origem do padrão. Poderá ser um dos seguintes valores:"

#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr "Topo-esquerda"

#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr "Topo"

#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr "Topo-direita"

#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr "Esquerda"

#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr "Centro"

#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr "Direita"

#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr "Fundo-esquerda"

#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr "Fundo"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr "Ponto de referência:"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr "Fundo-direita."

#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr "Para alguns ajustes adicionais, defina como uma percentagem."

#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr "X:"

#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr "Deslocamento na coordenada em X, logo na horizontal."

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr "Deslocamento do Ponto de Referência:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr "Y:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr "Deslocamento na coordenada em Y, logo na vertical."

#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr "Deslocamento do Padrão:"

#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr "O deslocamento do padrão, caso este seja repetido lado-a-lado."

#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr "Ajusta de forma fina o tamanho do padrão."

#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr "L:"

#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr "A largura em pixels."

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr "Tamanho do Padrão:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr "A:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr "A altura em pixels."

#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
"E chegamos agora aos :guilabel:`Padrões`, que é uma área com mini-padrões, "
"onde poderá escolher o padrão usado para o preenchimento."
