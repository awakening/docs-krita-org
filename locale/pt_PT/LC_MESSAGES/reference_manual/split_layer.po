# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:17+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: images image Kritamouseleft alt icons colorizemask\n"
"X-POFile-SpellExtra: Krita kbd ref mouseleft\n"

#: ../../<generated>:1
msgid "Palette to use for naming the layers"
msgstr "Paleta a usar para dar nomes às camadas"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../reference_manual/split_layer.rst:1
msgid "The Split Layer functionality in Krita"
msgstr "A funcionalidade de Divisão de Camadas no Krita"

#: ../../reference_manual/split_layer.rst:10
msgid "Splitting"
msgstr "Divisão"

#: ../../reference_manual/split_layer.rst:10
msgid "Color Islands"
msgstr "Ilhas de Cores"

#: ../../reference_manual/split_layer.rst:15
msgid "Split Layer"
msgstr "Dividir a Camada"

#: ../../reference_manual/split_layer.rst:16
msgid ""
"Splits a layer according to color. This is useful in combination with the :"
"ref:`colorize_mask` and the :kbd:`R +` |mouseleft| shortcut to select layers "
"at the cursor."
msgstr ""
"Divide uma camada de acordo com a cor. Isto é útil em conjunto com a opção :"
"ref:`colorize_mask` e :kbd:`R` + |mouseleft| para seleccionar as camadas no "
"cursor."

#: ../../reference_manual/split_layer.rst:18
msgid "Put all new layers in a group layer"
msgstr "Colocar todas as camadas novas numa camada de grupo"

#: ../../reference_manual/split_layer.rst:19
msgid "Put all the result layers into a new group."
msgstr "Coloca todas as camadas do resultado num novo grupo."

#: ../../reference_manual/split_layer.rst:20
msgid "Put every layer in its own, separate group layer"
msgstr "Colocar todas as camadas na sua camada de grupo própria e separada"

#: ../../reference_manual/split_layer.rst:21
msgid "Put each layer into its own group."
msgstr "Coloca cada camada no seu próprio grupo."

#: ../../reference_manual/split_layer.rst:22
msgid "Alpha-lock every new layer"
msgstr "Bloquear o 'alfa' de todas as camadas novas"

#: ../../reference_manual/split_layer.rst:23
msgid "Enable the alpha-lock for each layer so it is easier to color."
msgstr ""
"Activa o bloqueio do alfa para cada camada, para ser mais fácil colorir."

#: ../../reference_manual/split_layer.rst:24
msgid "Hide the original layer"
msgstr "Esconder a camada original"

#: ../../reference_manual/split_layer.rst:25
msgid ""
"Turns off the visibility on the original layer so you won't get confused."
msgstr ""
"Desliga a visibilidade da camada original para que não fique confundido."

#: ../../reference_manual/split_layer.rst:26
msgid "Sort layers by amount of non-transparent pixels"
msgstr "Ordenar as camadas pela quantidade de pixels não transparentes"

#: ../../reference_manual/split_layer.rst:27
msgid ""
"This ensures that the layers with large color swathes will end up at the top."
msgstr ""
"Isto garante que as camadas com grandes blocos de cores irão ficar no topo."

#: ../../reference_manual/split_layer.rst:28
msgid "Disregard opacity"
msgstr "Ignorar a opacidade"

#: ../../reference_manual/split_layer.rst:29
msgid "Whether to take into account transparency of a color."
msgstr "Se deve ter em conta a transparência de uma cor."

#: ../../reference_manual/split_layer.rst:30
msgid "Fuzziness"
msgstr "Difusão"

#: ../../reference_manual/split_layer.rst:31
msgid ""
"How precise the algorithm should be. The larger the fuzziness, the less "
"precise the algorithm will be. This is necessary for splitting layers with "
"anti-aliasing, because otherwise you would end up with a separate layer for "
"each tiny pixel."
msgstr ""
"Qual a precisão que deverá ter o algoritmo. Quanto maior a difusão, menos "
"exacto será o algoritmo. Isto é necessário para dividir as camadas com "
"suavização, dado que em caso contrário iria ficar com uma camada separada "
"para cada conjunto pequeno de pixels."

#: ../../reference_manual/split_layer.rst:33
msgid ""
"Select the palette to use for naming. Krita will compare the main color of a "
"layer to each color in the palette to get the most appropriate name for it."
msgstr ""
"Seleccione a paleta a usar para os nomes. O Krita irá comparar a cor "
"principal de uma camada com cada uma das cores na paleta para obter o nome "
"mais apropriado para ela."
