msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___drawing_tablets.pot\n"

#: ../../user_manual/drawing_tablets.rst:0
msgid ".. image:: images/Krita_tablet_stylus.png"
msgstr ""

#: ../../user_manual/drawing_tablets.rst:1
msgid ""
"Basic page describing drawing tablets, how to set them up for Krita and how "
"to troubleshoot common tablet issues."
msgstr ""
"数位板使用基础页面。介绍如何针对 Krita 配置数位板，以及如何排除常见数位板问"
"题。"

#: ../../user_manual/drawing_tablets.rst:13
msgid "Tablets"
msgstr ""

#: ../../user_manual/drawing_tablets.rst:18
msgid "Drawing Tablets"
msgstr "数位板"

#: ../../user_manual/drawing_tablets.rst:20
msgid ""
"This page is about drawing tablets, what they are, how they work, and where "
"things can go wrong."
msgstr "本页面介绍了数位板的用途、工作原理和遇到问题时的排除办法。"

#: ../../user_manual/drawing_tablets.rst:24
msgid "What are tablets?"
msgstr "数位板是什么"

#: ../../user_manual/drawing_tablets.rst:26
msgid ""
"Drawing with a mouse can be unintuitive and difficult compared to pencil and "
"paper. Even worse, extended mouse use can result in carpal tunnel syndrome. "
"That’s why most people who draw digitally use a specialized piece of "
"hardware known as a drawing tablet."
msgstr ""
"与笔和纸相比，用鼠标画画既不直观，又不顺手。更糟糕的是，长时间使用鼠标可能会"
"导致腕管综合症。这就是为什么大多数从事数字绘画的人会使用一种被称为“数位板”的"
"专用输入设备。"

#: ../../user_manual/drawing_tablets.rst:32
msgid ".. image:: images/Krita_tablet_types.png"
msgstr ""

#: ../../user_manual/drawing_tablets.rst:33
msgid ""
"A drawing tablet is a piece of hardware that you can plug into your machine, "
"much like a keyboard or mouse. It usually looks like a plastic pad, with a "
"stylus. Another popular format is a computer monitor with stylus used to "
"draw directly on the screen. These are better to use than a mouse because "
"it’s more natural to draw with a stylus and generally better for your wrists."
msgstr ""
"数位板和键盘、鼠标一样，是接在计算机上的外部输入设备。大部分数位板看上去像块"
"塑料板，配有一支可以感应光标位置和输入压力的压感笔，还有一些则是可以直接在屏"
"幕上用压感笔作画的数位屏。它们在绘画时比鼠标感觉更自然，对手腕的劳损也更小。"

#: ../../user_manual/drawing_tablets.rst:40
msgid ""
"With a properly installed tablet stylus, Krita can use information like "
"pressure sensitivity, allowing you to make strokes that get bigger or "
"smaller depending on the pressure you put on them, to create richer and more "
"interesting strokes."
msgstr ""
"数位板和压感笔正确安装后，Krita 可以从它们那里获得压力等传感器信息，你可以通"
"过施加的压力控制笔画的粗细，或者营造其他丰富有趣的笔触。"

#: ../../user_manual/drawing_tablets.rst:46
msgid ""
"Sometimes, people confuse finger-touch styluses with a proper tablet. You "
"can tell the difference because a drawing tablet stylus usually has a pointy "
"nib, while a stylus made for finger-touch has a big rubbery round nib, like "
"a finger. These tablets may not give good results and a pressure-sensitive "
"tablet is recommended."
msgstr ""
"模拟手指触摸的电容笔和真正的数位板压感笔不是一回事。一般来说，数位板的压感笔"
"配有一个又硬又尖的笔头，而模拟手指触摸的电容笔配有粗大的圆形橡胶笔头，它就像"
"指尖一样有弹性。电容笔的绘画效果一般都不太好，所以我们建议使用真正的具有压力"
"传感器的数位板。"

#: ../../user_manual/drawing_tablets.rst:51
msgid "Drivers and Pressure Sensitivity"
msgstr "驱动程序和压力感应"

#: ../../user_manual/drawing_tablets.rst:53
msgid ""
"So you have bought a tablet, a real drawing tablet. And you wanna get it to "
"work with Krita! So you plug in the USB cable, start up Krita and... It "
"doesn’t work! Or well, you can make strokes, but that pressure sensitivity "
"you heard so much about doesn’t seem to work."
msgstr ""
"现在，你买了一款真正的数位板，迫不及待地想要在 Krita 里面使用它。你把数位板插"
"入了 USB 接口，启动了 Krita……可这貌似并没有什么卵用。笔画虽然可以画得出来，可"
"是你朝思暮想的压力感应却偏偏欠奉。"

#: ../../user_manual/drawing_tablets.rst:58
msgid ""
"This is because you need to install a program called a ‘driver’. Usually you "
"can find the driver on a CD that was delivered alongside your tablet, or on "
"the website of the manufacturer. Go install it, and while you wait, we’ll go "
"into the details of what it is!"
msgstr ""
"这是因为你还没有安装一个叫做“驱动程序”的东西。驱动程序一般会包含在数位板附带"
"的光盘里，制造商也会在网站上提供驱动程序下载。执行它的安装程序之后，可能还要"
"等待一段时间才能安装完毕。让我们利用这点时间来介绍一下驱动程序是何方神圣吧！"

#: ../../user_manual/drawing_tablets.rst:63
msgid ""
"Running on your computer is a basic system doing all the tricky bits of "
"running a computer for you. This is the operating system, or OS. Most people "
"use an operating system called Windows, but people on an Apple device have "
"an operating system called MacOS, and some people, including many of the "
"developers use a system called Linux."
msgstr ""
"每台计算机都安装有一个基本的软件系统，它们代替用户执行一些深奥繁琐的技术性操"
"作，这就是所谓的操作系统，简称 OS。绝大多数人使用的操作系统是 Windows，苹果设"
"备的操作系统一般是 MacOS，而其余的一些人，包括许多程序员，习惯使用 Linux。"

#: ../../user_manual/drawing_tablets.rst:69
msgid ""
"The base principle of all of these systems is the same though. You would "
"like to run programs like Krita, called software, on your computer, and you "
"want Krita to be able to communicate with the hardware, like your drawing "
"tablet. But to have those two communicate can be really difficult - so the "
"operating system, works as a glue between the two."
msgstr ""
"虽然这几个操作系统各有特点，但它们的工作原理却是大同小异。你想做的是在计算机"
"上运行 Krita 这样的应用软件，然后让 Krita 与数位板这样的外设硬件进行沟通。然"
"而应用软件和硬件之间的直接沟通非常困难，操作系统的作用就是在两者之间建立起一"
"座桥梁。"

#: ../../user_manual/drawing_tablets.rst:75
msgid ""
"Whenever you start Krita, Krita will first make connections with the "
"operating system, so it can ask it for a lot of these things: It would like "
"to display things, and use the memory, and so on. Most importantly, it would "
"like to get information from the tablet!"
msgstr ""
"每当你启动 Krita 之后，Krita 首先会与操作系统建立联系，要求它进行显示图像、调"
"用内存等各种操作。不过现在最重要的是，Krita 要从数位板获取信息！"

#: ../../user_manual/drawing_tablets.rst:81
msgid ".. image:: images/Krita_tablet_drivermissing.png"
msgstr ""

#: ../../user_manual/drawing_tablets.rst:82
msgid ""
"But it can’t! Turns out your operating system doesn’t know much about "
"tablets. That’s what drivers are for. Installing a driver gives the "
"operating system enough information so the OS can provide Krita with the "
"right information about the tablet. The hardware manufacturer's job is to "
"write a proper driver for each operating system."
msgstr ""
"然而操作系统表示它做不到，因为它并不了解你的数位板是如何工作的。只有安装了驱"
"动程序，操作系统才可以正确地识别数位板，然后给 Krita 提供所需的数位板信息。一"
"般来说，数位板的制造商会负责为各个操作系统提供合适的驱动程序。"

#: ../../user_manual/drawing_tablets.rst:89
msgid ""
"Because drivers modify the operating system a little, you will always need "
"to restart your computer when installing or deinstalling a driver, so don’t "
"forget to do this! Conversely, because Krita isn’t a driver, you don’t need "
"to even deinstall it to reset the configuration, just rename or delete the "
"configuration file."
msgstr ""
"驱动程序会对操作系统进行一些更改，所以在安装或者卸载驱动之后必需重新启动计算"
"机才能生效，千万别忘了做这一步！而与之相对的是，Krita 并非驱动程序，如需重置 "
"Krita 的设置，你只需重命名或者删除它的配置文件，无需卸载之后重新安装。"

#: ../../user_manual/drawing_tablets.rst:92
msgid "Where it can go wrong: Windows"
msgstr "Windows 环境下可能发生的问题"

#: ../../user_manual/drawing_tablets.rst:94
msgid ""
"Krita automatically connects to your tablet if the drivers are installed. "
"When things go wrong, usually the problem isn't with Krita."
msgstr ""
"如果驱动已经正确安装，Krita 会自动连接到数位板。数位板要是工作不正常，一般都"
"不是 Krita 造成的问题。"

#: ../../user_manual/drawing_tablets.rst:98
msgid "Surface pro tablets need two drivers"
msgstr "Surface Pro 类笔式平板电脑的两种数位板驱动程序"

#: ../../user_manual/drawing_tablets.rst:100
msgid ""
"Certain tablets using n-trig, like the Surface Pro, have two types of "
"drivers. One is native, n-trig and the other one is called wintab. Since "
"3.3, Krita can use Windows Ink style drivers, just go to :menuselection:"
"`Settings --> Configure Krita --> Tablet Settings` and toggle the :guilabel:"
"`Windows 8+ Pointer Input` there. You don't need to install the wintab "
"drivers anymore for n-trig based pens."
msgstr ""
"包括 Surface Pro 在内的许多笔式平板电脑使用的是 N-trig 技术，它们具备两种驱动"
"程序。一种是 N-trig 体系特有的 Windows Ink 驱动，另一种则是传统的 Wintab 驱"
"动。Windows Ink 技术由操作系统提供原生支持，使用基于 N-Trig 技术的压感笔时无"
"需安装 Wintab 驱动。Krita 从 3.3 版开始支持 Windows Ink 驱动程序，可在菜单栏"
"的 :menuselection:`设置 --> 配置 Krita --> 数位板设置` 里面勾选 :guilabel:"
"`Windows 8+ 指针输入` 。"

#: ../../user_manual/drawing_tablets.rst:108
msgid "Windows 10 updates"
msgstr "Windows 10 更新"

#: ../../user_manual/drawing_tablets.rst:110
msgid ""
"Sometimes a Windows 10 update can mess up tablet drivers. In that case, "
"reinstalling the drivers should work."
msgstr ""
"Windows 10 推送的更新有时会损坏数位板的驱动程序，重新安装驱动程序一般可以解决"
"问题。"

#: ../../user_manual/drawing_tablets.rst:114
msgid "Wacom Tablets"
msgstr "Wacom 数位板"

#: ../../user_manual/drawing_tablets.rst:116
msgid "There are two known problems with Wacom tablets and Windows."
msgstr "Wacom 数位板在 Windows 下面有两个已知问题。"

#: ../../user_manual/drawing_tablets.rst:118
msgid ""
"The first is that if you have customized the driver settings, then "
"sometimes, often after a driver update, but that is not necessary, the "
"driver breaks. Resetting the driver to the default settings and then loading "
"your settings from a backup will solve this problem."
msgstr ""
"首先，如果你修改过驱动程序设置，那么在更新驱动程序之后它可能会发生故障。这时"
"先把驱动程序重置为默认设置，然后载入备份的设置，问题就可以得到解决。"

#: ../../user_manual/drawing_tablets.rst:123
msgid ""
"The second is that for some reason it might be necessary to change the "
"display priority order. You might have to make your Cintiq screen your "
"primary screen, or, on the other hand, make it the secondary screen. Double "
"check in the Wacom settings utility that the tablet in the Cintiq is "
"associated with the Cintiq screen."
msgstr ""
"其次，你可能需要更改显示器的优先顺序。在某些情况下你要把 Cintiq 设为主屏幕，"
"而另外一些情况下你要把它设成副屏幕。打开 Wacom 数位板属性，检查 Cintiq 的数位"
"板是否和它的屏幕正确关联。"

#: ../../user_manual/drawing_tablets.rst:130
msgid "Broken Drivers"
msgstr "糟糕的驱动程序"

#: ../../user_manual/drawing_tablets.rst:132
msgid ""
"Tablet drivers need to be made by the manufacturer. Sometimes, with really "
"cheap tablets, the hardware is fine, but the driver is badly written, which "
"means that the driver just doesn’t work well. We cannot do anything about "
"this, sadly. You will have to send a complaint to the manufacturer for this, "
"or buy a better tablet with better quality drivers."
msgstr ""
"数位板的驱动程序是由制造商编写的。一些非常廉价的数位板，虽然它们的硬件还行，"
"但配套的驱动程序却非常糟糕，无法正常工作。我们对此无能为力。你要么联系制造商"
"反映问题，要么重新购买一块配有良好驱动程序的数位板。"

#: ../../user_manual/drawing_tablets.rst:140
msgid "Conflicting Drivers"
msgstr "驱动程序冲突"

#: ../../user_manual/drawing_tablets.rst:142
msgid ""
"On Windows, you can only have a single wintab-style driver installed at a "
"time. So be sure to deinstall the previous driver before installing the one "
"that comes with the tablet you want to use. Other operating systems are a "
"bit better about this, but even Linux, where the drivers are often "
"preinstalled, can't run two tablets with different drivers at once."
msgstr ""
"Windows 通常只允许使用一个 Wintab 驱动程序。如果需要更换数位板硬件，请记得先"
"卸载旧数位板的驱动程序，然后再安装新数位板的驱动程序。其他操作系统的表现会好"
"一些，但即使是内建了大部分数位板驱动的 Linux，也无法同时识别两块使用了不同驱"
"动程序的数位板。"

#: ../../user_manual/drawing_tablets.rst:150
msgid "Interfering software"
msgstr "软件干扰"

#: ../../user_manual/drawing_tablets.rst:152
msgid ""
"Sometimes, there's software that tries to make a security layer between "
"Krita and the operating system. Sandboxie is an example of this. However, "
"Krita cannot always connect to certain parts of the operating system while "
"sandboxed, so it will often break in programs like sandboxie. Similarly, "
"certain mouse software, like Razer utilities can also affect whether Krita "
"can talk to the operating system, converting tablet information to mouse "
"information. This type of software should be configured to leave Krita "
"alone, or be deinstalled."
msgstr ""
"有一些软件会在 Krita 和操作系统之间建立一个安全层，Sandboxie 就是个例子。然"
"而 Krita 无法在沙箱中与操作系统建立稳定的联系，所以在 Sandboxie 等软件中 "
"Krita 会频繁出错。还有一些鼠标应用程序，如 Razer 实用程序等，它们会把数位板信"
"息转换为鼠标信息，影响 Krita 和操作系统之间的正常通信。这些软件，要么把它们设"
"置为不影响 Krita，要么干脆把它们给卸载掉。"

#: ../../user_manual/drawing_tablets.rst:161
msgid ""
"The following software has been reported to interfere with tablet events to "
"Krita:"
msgstr "根据用户反馈信息，下列软件会干扰 Krita 读取数位板事件："

#: ../../user_manual/drawing_tablets.rst:164
msgid "Sandboxie"
msgstr "Sandboxie"

#: ../../user_manual/drawing_tablets.rst:165
msgid "Razer mouse utilities"
msgstr "Razer 鼠标实用程序"

#: ../../user_manual/drawing_tablets.rst:166
msgid "AMD catalyst “game mode” (this broke the right click for someone)"
msgstr "AMD Catalyst 驱动程序的“游戏模式” (可能造成右键失效)"

#: ../../user_manual/drawing_tablets.rst:169
msgid "Flicks (Wait circle showing up and then calling the popup palette)"
msgstr "Flicks 手势 (出现等待圆圈，然后弹出浮动画具板)"

#: ../../user_manual/drawing_tablets.rst:171
msgid ""
"If you have a situation where trying to draw keeps bringing up the pop-up "
"palette on Windows, then the problem might be flicks. These are a type of "
"gesture, a bit of Windows functionality that allows you to make a motion to "
"serve as a keyboard shortcut. Windows automatically turns these on when you "
"install tablet drivers, because the people who made this part of Windows "
"forgot that people also draw with computers. So you will need to turn it off "
"in the Windows flicks configuration."
msgstr ""
"如果你发现在 Windows 下面绘画时老是弹出浮动画具板，那么犯人很可能是 Flicks 手"
"势。Flicks 是 Windows 的一种手势功能，它把光标动作转换成键盘快捷键。Windows "
"会在安装数位板驱动之后自动启用此功能，设计此功能的高僧是不是忘了人们是用数位"
"板来画画的？你可以在 Windows Flicks 配置对话框中关闭此功能。"

#: ../../user_manual/drawing_tablets.rst:180
msgid "Wacom Double Click Sensitivity (Straight starts of lines)"
msgstr "Wacom 双击时笔尖间距 (笔画的起始部分总是画成直线)"

#: ../../user_manual/drawing_tablets.rst:182
msgid ""
"If you experience an issue where the start of the stroke is straight, and "
"have a wacom tablet, it could be caused by the Wacom driver double-click "
"detection."
msgstr ""
"如果你使用 Wacom 数位板，遇到了笔画的开头部分总是画成直线的情况，它可能是 "
"Wacom 驱动程序的“双击时笔尖间距”选项出了问题。"

#: ../../user_manual/drawing_tablets.rst:186
msgid ""
"To fix this, go to the Wacom settings utility and lower the double click "
"sensitivity."
msgstr "要修复这个问题，打开 Wacom 数位板属性，调低“双击时笔尖间距”。"

#: ../../user_manual/drawing_tablets.rst:190
msgid "Supported Tablets"
msgstr "支持的数位板"

#: ../../user_manual/drawing_tablets.rst:192
msgid ""
"Supported tablets are the ones of which Krita developers have a version "
"themselves, so they can reliably fix bugs with them. :ref:`We maintain a "
"list of those here <list_supported_tablets>`."
msgstr ""
"Krita 支持的数位板一般都是我们的开发人员手头上已有的设备，只有这样我们才能可"
"靠地修复它们的问题。我们也整理了一个 :ref:`数位板支持情况的列表 "
"<list_supported_tablets>` 。"
