# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:45+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/lossy_lossless.rst:1
msgid "The difference between lossy and lossless compression."
msgstr "Skillnaden mellan destruktiv och förlustfri bildkomprimering."

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "lossy"
msgstr "destruktiv"

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "lossless"
msgstr "förlustfri"

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "compression"
msgstr "komprimering"

#: ../../general_concepts/file_formats/lossy_lossless.rst:17
msgid "Lossy and Lossless Image Compression"
msgstr "Destruktiv och förlustfri bildkomprimering"

#: ../../general_concepts/file_formats/lossy_lossless.rst:20
msgid ""
"When we compress a file, we do this because we want to temporarily make it "
"smaller (like for sending over email), or we want to permanently make it "
"smaller (like for showing images on the internet)."
msgstr ""
"När en fil komprimeras gör vi det eftersom vi tillfälligt vill göra den "
"mindre (såsom att skicka den via e-post), eller vi vill göra den mindre "
"(såsom att visa bilder på Internet)."

#: ../../general_concepts/file_formats/lossy_lossless.rst:22
msgid ""
"*Lossless* compression techniques are for when we want to *temporarily* "
"reduce information. As the name implies, they compress without losing "
"information. In text, the use of abbreviations is a good example of a "
"lossless compression technique. Everyone knows 'etc.' expands to 'etcetera', "
"meaning that you can half the 8 character long 'etcetera' to the four "
"character long 'etc.'."
msgstr ""
"*Förlustfria* komprimeringstekniker är till för när vi *tillfälligt* vill "
"reducera informationen. Som namnet anger, komprimerar de utan att förlora "
"information. I text är användning av förkortningar ett bra exempel på en "
"förlustfri komprimeringsteknik. Alla vet att 'etc.' expanderas till "
"'etcetera', vilket betyder att man kan halvera det 8 tecken långa 'etcetera' "
"till det fyra tecken långa 'etc.'."

#: ../../general_concepts/file_formats/lossy_lossless.rst:24
msgid ""
"Within image formats, examples of such compression is by for example "
"'indexed' color, where we make a list of available colors in an image, and "
"then assign a single number to them. Then, when describing the pixels, we "
"only write down said number, so that we don't need to write the color "
"definition over and over."
msgstr ""
"Bland bildformat är exempel på sådan komprimering exempelvis 'indexerade' "
"färger, där en lista över tillgängliga färger i en bild skapas, och ett enda "
"tal tilldelas till dem. När bildpunkterna sedan beskrivs, skriver man bara "
"ner ovannämnda tal, så att färgdefinitionen inte behöver skrivas gång på "
"gång."

#: ../../general_concepts/file_formats/lossy_lossless.rst:26
msgid ""
"*Lossy* compression techniques are for when we want to *permanently* reduce "
"the file size of an image. This is necessary for final products where having "
"a small filesize is preferable such as a website. That the image will not be "
"edited anymore after this allows for the use of the context of a pixel to be "
"taken into account when compressing, meaning that we can rely on "
"psychological and statistical tricks."
msgstr ""
"*Destruktiva* komprimeringstekniker är till för att *permanent* reducera "
"filstorleken hos en bild. Det är nödvändigt för slutprodukter där man "
"föredrar att ha en liten filstorlek, såsom en webbplats. Att bilden inte "
"kommer att redigeras längre efteråt tillåter att hänsyn tas till användning "
"av en bildpunkts sammanhang, vilket betyder att vi kan förlita oss på "
"psykologiska och statistiska trick."

#: ../../general_concepts/file_formats/lossy_lossless.rst:28
msgid ""
"One of the primary things JPEG for example does is chroma sub-sampling, that "
"is, to split up the image into a grayscale and two color versions (one "
"containing all red-green contrast and the other containing all blue-yellow "
"contrast), and then it makes the latter two versions smaller. This works "
"because humans are much more sensitive to differences in lightness than we "
"are to differences in hue and saturation."
msgstr ""
"En av de primära saker som exempelvis JPEG gör är kroma delsampling, det "
"vill säga dela upp bilden i en gråskala och två färgversioner (en som "
"innehåller all röd-grön kontrast och en som innehåller blå-gul kontrast), "
"och gör därefter de senare två versionerna mindre. Det fungerar eftersom "
"människor är mycket känsligare för skillnader i ljusstyrka än vi är när det "
"gäller färgton och färgmättnad."

#: ../../general_concepts/file_formats/lossy_lossless.rst:30
msgid ""
"Another thing it does is to use cosine waves to describe contrasts in an "
"image. What this means is that JPEG and other lossy formats using this are "
"*very good at describing gradients, but not very good at describing sharp "
"contrasts*."
msgstr ""
"En annan sak som görs är att använda cosinusvågor för att beskriva kontrast "
"på en bild. Vad det betyder är att JPEG och andra destruktiva format som "
"använder det är *mycket bra på att beskriva toning, men inte särskilt bra på "
"att beskriva skarpa kontraster*."

#: ../../general_concepts/file_formats/lossy_lossless.rst:32
msgid ""
"Conversely, lossless image compression techniques are *really good at "
"describing images with few colors thus sharp contrasts, but are not good to "
"compress images with a lot of gradients*."
msgstr ""
"Omvänt är förlustfria bildkomprimeringstekniker *verkligt bra på att "
"beskriva bilder med få färger, och därmed skarpa kontraster, men är inte bra "
"på att komprimera bilder med mycket toning*."

#: ../../general_concepts/file_formats/lossy_lossless.rst:34
msgid ""
"Another big difference between lossy and lossless images is that lossy file "
"formats will degrade if you re-encode them, that is, if you load a JPEG into "
"Krita edit a little, resave, edit a little, resave, each subsequent save "
"will lose some data. This is a fundamental part of lossy image compression, "
"and the primary reason we use working files."
msgstr ""
"En annan stor skillnad mellan förlustfria och destruktiva bilder är att "
"destruktiva filformat degraderar om man kodar om dem, det vill säga om en "
"JPEG läses in i Krita, redigeras lite grand, sparas igen, redigeras lite "
"grand, sparas igen, går en viss mängd data förlorad varje efterföljande gång "
"den sparas. Det är en grundläggande del av destruktiv bildkomprimering, och "
"huvudorsaken vi använder arbetsfiler."

#: ../../general_concepts/file_formats/lossy_lossless.rst:38
msgid ""
"If you're interested in different compression techniques, `Wikipedia's "
"page(s) on image compression <https://en.wikipedia.org/wiki/"
"Image_compression>`_ are very good, if not a little technical."
msgstr ""
"Om man är intresserad av olika komprimeringstekniker, är `Wikipedias sida om "
"bildkomprimering <https://sv.wikipedia.org/wiki/Bildkompression>`_ bra, även "
"om den inte är så teknisk."
