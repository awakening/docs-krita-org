# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:43+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_psd.rst:1
msgid "The Photoshop file format as exported by Krita."
msgstr "Photoshop-filformatet som exporteras av Krita."

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "*.psd"
msgstr "*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "PSD"
msgstr "PSD"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "Photoshop Document"
msgstr "Photoshop-dokument"

#: ../../general_concepts/file_formats/file_psd.rst:15
msgid "\\*.psd"
msgstr "\\*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:17
msgid ""
"``.psd`` is Photoshop's internal file format. For some reason, people like "
"to use it as an interchange format, even though it is not designed for this."
msgstr ""
"Det interna filformatet i Photoshop är ``.psd``. Av någon anledning tycker "
"folk om att använda det som ett utbytesformat, även om det inte är "
"konstruerat för det."

#: ../../general_concepts/file_formats/file_psd.rst:19
msgid ""
"``.psd``, unlike actual interchange formats like :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` and :ref:`file_svg` doesn't "
"have an official spec online. Which means that it needs to be reverse "
"engineered. Furthermore, as an internal file format, it doesn't have much of "
"a philosophy to its structure, as it's only purpose is to save what "
"Photoshop is busy with, or rather, what all the past versions of Photoshop "
"have been busy with. This means that the inside of a PSD looks somewhat like "
"Photoshop's virtual brains, and PSD is in general a very disliked file-"
"format."
msgstr ""
"I motsats till verkliga utbytesformat, som :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` och :ref:`file_svg`, har ``."
"psd`` inte en officiell specifikation på nätet, vilket betyder att man måste "
"använda omvänd teknik. Dessutom, som ett internt filformat, har det inte "
"mycket filosofi bakom sin struktur, eftersom dess enda syfte är att spara "
"vad Photoshop är upptaget med, eller snarare vad alla tidigare versioner av "
"Photoshop har varit upptagna med. Det betyder att insidan av en PSD i viss "
"mån ser ut som Photoshops virtuella hjärna, och PSD är i allmänhet ett "
"mycket illa omtyckt filformat."

#: ../../general_concepts/file_formats/file_psd.rst:21
msgid ""
"Due to ``.psd`` being used as an interchange format, this leads to confusion "
"amongst people using these programs, as to why not all programs support "
"opening these. Sometimes, you might even see users saying that a certain "
"program is terrible because it doesn't support opening PSDs properly. But as "
"PSD is an internal file-format without online specs, it is impossible to "
"have any program outside it support it 100%."
msgstr ""
"På grund av att ``.psd`` används som ett utbytesformat, leder det till att "
"folk som använder programmen blir förbryllade över varför inte alla program "
"stödjer att öppna det. Ibland kan man till och med se användare som påstår "
"att ett visst program är förfärligt eftersom det inte stödjer PSD "
"ordentligt. Men eftersom PSD är ett internt filformat utan specifikation på "
"nätet, är det omöjligt att något program utanför stödjer det till 100 "
"procent."

#: ../../general_concepts/file_formats/file_psd.rst:23
msgid ""
"Krita supports loading and saving raster layers, blending modes, "
"layerstyles, layer groups, and transparency masks from PSD. It will likely "
"never support vector and text layers, as these are just too difficult to "
"program properly."
msgstr ""
"Krita stöder att läsa in och spara rastreringslager, blandningslägen, "
"lagerstilar, lagergrupper och genomskinlighetsmasker från PSD. Det kommer "
"troligen aldrig stödja vektor- och textlager, eftersom de helt enkelt är för "
"svåra att programmera på ett riktigt sätt."

#: ../../general_concepts/file_formats/file_psd.rst:25
msgid ""
"We recommend using any other file format instead of PSD if possible, with a "
"strong preference towards :ref:`file_ora` or :ref:`file_tif`."
msgstr ""
"Vi rekommenderar att vilket annat filformat som helst används istället för "
"PSD, om möjligt, med en stark preferens för :ref:`file_ora` eller :ref:"
"`file_tif`."

#: ../../general_concepts/file_formats/file_psd.rst:27
msgid ""
"As a working file format, PSDs can be expected to become very heavy and most "
"websites won't accept them."
msgstr ""
"Som arbetsfilformat kan PSD förväntas vara mycket tungrott, och de flesta "
"webbplatser accepterar det inte."
