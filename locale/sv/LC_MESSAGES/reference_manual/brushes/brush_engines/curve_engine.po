# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-23 15:02+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:1
msgid "The Curve Brush Engine manual page."
msgstr "Handbokssidan för kurvpenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:18
msgid "Curve Brush Engine"
msgstr "Kurvpenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:21
msgid ".. image:: images/icons/curvebrush.svg"
msgstr ".. image:: images/icons/curvebrush.svg"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:22
msgid ""
"The curve brush is a brush engine which creates strokes made of evenly "
"spaced lines. It has, among other things been used as a replacement for "
"pressure sensitive strokes in lieu of a tablet."
msgstr ""
"Kurvpenseln är ett penselgränssnitt som skapar penseldrag som består av "
"jämnt fördelade linjer. Den har bland annat används som en ersättning för "
"tryckkänsliga drag i avsaknad av ritplatta."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:25
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:27
msgid ""
"First off, the line produced by the Curve brush is made up of 2 sections:"
msgstr ""
"För det första, består linjen som skapas av kurvpenseln av två sektioner:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:29
msgid "The connection line, which is the main line drawn by your mouse"
msgstr "Sammanbindningslinjen, som är huvudlinjen som ritas med musen"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:31
msgid ""
"The curve lines I think, which are the extra fancy lines that form at "
"curves. The curve lines are formed by connecting one point of the curve to a "
"point earlier on the curve. This also means that if you are drawing a "
"straight line, these lines won't be visible, since they'll overlap with the "
"connection line. Drawing faster gives you wider curves areas."
msgstr ""
"Kurvlinjerna tror jag, som är de extra fina linjerna som skapas vid kurvor. "
"Kurvlinjerna skapas genom att ansluta en punkt på kurvan till en punkt "
"tidigare på kurvan. Det betyder också att om man ritar en rät linje, är de "
"inte synliga, eftersom de överlappar med sammanbindningslinjen. Att rita "
"snabbare ger bredare kurvområden."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:35
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:36
msgid ""
"You have access to 3 settings from the Lines tab, as well as 2 corresponding "
"dynamics:"
msgstr ""
"Man har tillgång till tre inställningar från fliken Linjer, samt två "
"motsvarande typer av dynamik:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:38
msgid ""
"Line width: this applies to both the connection line and the curve lines."
msgstr "Linjebredd: Den gäller både sammanbindningslinjen och kurvlinjerna."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:40
msgid "Line width dynamics: use this to vary line width dynamically."
msgstr "Linjebreddynamik: Använd den för att variera linjebredden dynamiskt."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:42
msgid ""
"History size: this determines the distance for the formation of curve lines."
msgstr "Historikstorlek: Den bestämmer avståndet för att forma kurvlinjerna."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:44
msgid ""
"If you set this at low values, then the curve lines can only form over a "
"small distances, so they won't be too visible."
msgstr ""
"Om man ställer in den till små värden kan kurvlinjerna bara formas över ett "
"kort avstånd, så de blir inte särskilt synliga."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:45
msgid ""
"On the other hand, if you set this value too high, the curve lines will only "
"start forming relatively \"late\"."
msgstr ""
"Å andra sidan, om värdet ställs in för stort, börjar kurvlinjerna bara "
"formas relativt \"sent\"."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:46
msgid ""
"So in fact, you'll get maximum curve lines area with a mid-value of say... "
"40~60, which is about the default value. Unless you're drawing at really "
"high resolutions."
msgstr ""
"Så i själva verket får man maximalt område med kurvlinjer med ett "
"mellanvärde på ungefär ... 40 - 60, vilket är ungefär förvalt värde. Om man "
"inte ritar med mycket hög upplösning."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:48
msgid ""
"Curves opacity: you can't set different line widths for the connection line "
"and the curve lines, but you can set a different opacity for the curve "
"lines. With low opacity, this will produce the illusion of thinner curve "
"lines."
msgstr ""
"Kurvogenomskinlighet: Man kan inte ställa in olika linjebredder för "
"sammanbindningslinjen och kurvlinjerna, men man kan ställa in en annan "
"ogenomskinlighet för kurvlinjerna. Med låg ogenomskinlighet, ger det en "
"illusion av tunnare kurvlinjer."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:50
msgid "Curves opacity dynamics: use this to vary Curves opacity dynamically."
msgstr ""
"Kurvogenomskinlighetsdynamik: Använd den för att variera ogenomskinligheten "
"dynamiskt."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:52
msgid "In addition, you have access to two checkboxes:"
msgstr "Dessutom har man tillgång till två kryssrutor:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:54
msgid ""
"Paint connection line, which toggles the visibility of the connection line"
msgstr "Rita sammanbindningslinjen, som ändrar anslutningslinjens synlighet"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:55
msgid ""
"Smoothing, which... I have no idea actually. I don't see any differences "
"with or without it. Maybe it's for tablets?"
msgstr ""
"Utjämning, som ... Jag har faktiskt ingen aning. Jag ser ingen skillnad med "
"eller utan det. Kanske för ritplattor?"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:58
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:60
msgid "Drawing variable-width lines"
msgstr "Rita linjer med variabel bredd"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:62
msgid ""
"And here's the only section of this tutorial that anyone cares about: pretty "
"lineart lines! For this:"
msgstr ""
"Och här är avsnittet av handledningen som någon bryr sig om: snygga "
"grafiklinjer! För sådana:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:64
msgid ""
"Use the Draw Dynamically mode: I tend to increase drag to at least 50. Vary "
"Mass and Drag until you get the feel that's most comfortable for you."
msgstr ""
"Använd dynamiskt ritläge. Jag brukar öka drag till minst 50. Variera massa "
"och drag tills den mest komfortabel känslan erhålls."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:67
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:68
msgid ""
"Set line width to a higher value (ex.: 5), then turn line width dynamics on:"
msgstr ""
"Ställ in linjebredden till ett större värde (t.ex. 5), och aktivera sedan "
"linjebreddsdynamik:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:70
msgid ""
"If you're a tablet user, just set this to Pressure (this should be selected "
"by default so just turn on the Line Width dynamics). I can't check myself, "
"but a tablet user confirmed to me that it works well enough with Draw "
"Dynamically."
msgstr ""
"Om du använder en ritplatta, ställ bara in det till Tryck (det ska normalt "
"vara inställt, så stäng bara av dynamik för linjebredd). Jag kan inte "
"kontrollera det själv, men en ritplatteanvändare bekräftade att det fungerar "
"bra nog med rita dynamiskt."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:71
msgid ""
"If you're a mouse user hoping to get variable line width, set the Line Width "
"dynamics to Speed."
msgstr ""
"Om du är musanvändare och hoppas få variabel linjebredd, ställ in "
"linjebreddsdynamiken till Hastighet."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:74
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:75
msgid ""
"Set Curves opacity to 0: This is the simplest way to turn off the Curve "
"lines. That said, leaving them on will get you more \"expressive\" lines."
msgstr ""
"Ställ in kurvans ogenomskinlighet till 0: Det är det enklaste sättet att "
"stänga av kurvlinjerna. Trots det, att lämna dem på ger mer \"uttrycksfulla"
"\" linjer."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:78
msgid "Additional tips:"
msgstr "Ytterligare tips:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:80
msgid "Zig-zag a lot if you want a lot of extra curves lines."
msgstr "Sicksacka mycket för att få många extra kurvlinjer."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:81
msgid ""
"Use smooth, sweeping motions when you're using Draw Dynamically with Line "
"Width set to Speed: abrupt speed transitions will cause abrupt size "
"transitions. It takes a bit of practice, and the thicker the line, the more "
"visible the deformities will be. Also, zoom in to increase control."
msgstr ""
"Använd jämna, svepande rörelser när rita dynamiskt används med linjebredd "
"inställd till hastighet: plötsliga hastighetsändringar orsakar plötsliga "
"ändringar av linjebredd. Det kräver en del övning, och ju tjockare linjen är "
"desto synligare blir missbildningarna. Zooma också för bättre kontroll."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:82
msgid ""
"If you need to vary between thin and thick lines, I suggest creating presets "
"of different widths, since you can't vary the base line width from the "
"canvas."
msgstr ""
"Om man behöver byta mellan tunna och tjocka linjer, föreslår jag att skapa "
"förinställningar med olika bredder, eftersom man inte kan variera "
"baslinjebredden från duken."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:84
msgid "Alternative:"
msgstr "Alternativ:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:86
msgid "Use the Draw Dynamically mode"
msgstr "Använd dynamiskt ritläge"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:87
msgid "Set Curves opacity to 100"
msgstr "Ställer in kurvors ogenomskinlighet till 100"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:88
msgid "Optionally decrease History size to about 30"
msgstr "Minskar valfritt historikstorleken till omkring 30"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:90
msgid ""
"The curve lines will fill out the area they cover completely, resulting in a "
"line with variable widths. Anyway, here are some comparisons:"
msgstr ""
"Kurvlinjerna fyller i området de täcker fullständigt, vilket resulterar i en "
"linje med variabel bredd. Hur som helst, här är några jämförelser:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:93
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:94
msgid "And here are examples of what you can do with this brush:"
msgstr "Och här är exempel på vad man kan göra med penseln:"
